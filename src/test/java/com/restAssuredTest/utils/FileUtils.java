package com.restAssuredTest.utils;

import java.io.InputStream;

public class FileUtils {

    public static InputStream readQueryFile(String fileNameFromQueriesDirectory) {
        return readResource("graphqlqueries/" + fileNameFromQueriesDirectory);
    }

    public static InputStream readResource(String fileNameFromResourcesDirectory) {
        return FileUtils.class.getClassLoader().getResourceAsStream(fileNameFromResourcesDirectory);
    }

}
