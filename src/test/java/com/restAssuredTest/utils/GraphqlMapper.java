package com.restAssuredTest.utils;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class GraphqlMapper {

    // todo: fix missing bean
//    @Autowired
    private ObjectMapper objectMapper = new ObjectMapper();

    public <T> T map(ObjectNode objectNode, String mainObjectName, Class<T> responseType) throws IOException {
        JsonNode data = objectNode.get("data").get(mainObjectName);

        return objectMapper.readValue(data.toString(), responseType);
    }

    public ObjectNode map(Object object) {
        return objectMapper.convertValue(object, ObjectNode.class);
    }

}
