package com.restAssuredTest.model.Media;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateMediaRequest {

    private int id;
    private MediaTitle title;

}
