package com.restAssuredTest.model.staff;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StaffName {

    private String first;
    private String last;
    private String full;
    @JsonProperty("native")
    private boolean nativeName;


}
