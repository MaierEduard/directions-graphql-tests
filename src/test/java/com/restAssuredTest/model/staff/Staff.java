package com.restAssuredTest.model.staff;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Staff {

    private int id;
    private String language;
    private StaffName name;

}