package com.restAssuredTest.steps.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.restAssuredTest.data.TestData;
import com.restAssuredTest.model.CreateUserRequest;
import com.restAssuredTest.model.User;
import com.restAssuredTest.utils.GraphqlMapper;
import com.vimalselvam.graphql.GraphqlTemplate;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static com.restAssuredTest.utils.FileUtils.readQueryFile;
import static org.hamcrest.MatcherAssert.assertThat;

public class UserSteps {

    @Autowired
    private TestData testData;

    @Autowired
    private GraphqlMapper graphqlMapper;

    // this should be autowired when we fix missing bean
    private ObjectMapper objectMapper = new ObjectMapper();

    @When("I get the user with id {int}")
    public void iGetTheUserWithId(int userId) throws IOException {
        // alternative for graphqlMapper.map(...)
        ObjectNode objectNodeAlt = objectMapper.createObjectNode();
        objectNodeAlt.put("id", userId);

//        CreateUserRequest createUserRequest = CreateUserRequest.builder()
//                .id(userId)
//                .build();

        ObjectNode objectNode = graphqlMapper.map(objectNodeAlt);

        String body = GraphqlTemplate.parseGraphql(readQueryFile("get-user.graphql"), objectNode);

        ObjectNode response = RestAssured.given()
                // todo: read baseUri from config file
                .baseUri("https://graphql.anilist.co")
                .body(body)
                .contentType(ContentType.JSON)
                .when()
                .post("/")
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .as(ObjectNode.class);

        User user = graphqlMapper.map(response, "User", User.class);

        System.out.println(user);
//        assertThat()
    }
}
