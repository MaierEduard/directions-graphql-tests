package com.restAssuredTest.steps.media;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.restAssuredTest.data.TestData;
import com.restAssuredTest.model.Media.CreateMediaRequest;
import com.restAssuredTest.utils.GraphqlMapper;
import com.vimalselvam.graphql.GraphqlTemplate;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static com.restAssuredTest.utils.FileUtils.readQueryFile;

public class MediaSteps {

    @Autowired
    private TestData testData;

    @Autowired
    private GraphqlMapper graphqlMapper;

    private ObjectMapper objectMapper = new ObjectMapper();

    @When("I get the media with id {int}")
    public void iGetTheMediaWithId(int mediaId) throws IOException {

        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("id", mediaId);

        String body = GraphqlTemplate.parseGraphql(readQueryFile("get-media-by-id.graphql"), objectNode);

        ObjectNode response = RestAssured.given()

                .baseUri("https://graphql.anilist.co")
                .body(body)
                .contentType(ContentType.JSON)
                .when()
                .post("/")
                .then()
                .log().all().statusCode(200).extract()
                .as(ObjectNode.class);

        CreateMediaRequest createMediaRequest = graphqlMapper.map(response, "Media", CreateMediaRequest.class);
        System.out.println(createMediaRequest);

    }
}
