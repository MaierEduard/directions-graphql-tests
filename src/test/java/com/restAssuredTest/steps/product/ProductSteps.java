package com.restAssuredTest.steps.product;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.restAssuredTest.data.TestData;
import com.restAssuredTest.model.product.Product;
import com.restAssuredTest.model.product.Products;
import com.restAssuredTest.utils.GraphqlMapper;
import com.vimalselvam.graphql.GraphqlTemplate;
import cucumber.api.java.en.When;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static com.restAssuredTest.utils.FileUtils.readQueryFile;

public class ProductSteps {

    @Autowired
    private TestData testData;

    @Autowired
    private GraphqlMapper graphqlMapper;

    private ObjectMapper objectMapper = new ObjectMapper();

    @When("I get the product whit id {int}")
    public void iGetTheProductWhitId(int productId) throws IOException {

        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("id", productId);

        String body = GraphqlTemplate.parseGraphql(readQueryFile("get-product-by-id.graphql"), objectNode);

        ObjectNode response = RestAssured.given()

                .baseUri("https://graphql.anilist.co")
                .body(body)
                .contentType(ContentType.JSON)
                .when()
                .post("/")
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .as(ObjectNode.class);
        Product product = graphqlMapper.map(response, "data", Product.class);
        System.out.println(product);

    }

    @When("I get the product whit id {string}")
    public void iGetTheProductWhitId(String productId) throws IOException {
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("id", productId);

        String body = GraphqlTemplate.parseGraphql(readQueryFile("get-product-by-id.graphql"), objectNode);

        ObjectNode response = RestAssured.given()

                .baseUri("https://graphql.anilist.co")
                .body(body)
                .contentType(ContentType.JSON)
                .when()
                .post("/")
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .as(ObjectNode.class);
        Products products = graphqlMapper.map(response, "products", Products.class);
        System.out.println(products);
    }

    @When("I get all the products")
    public void iGetAllTheProducts() throws IOException {


        String body = GraphqlTemplate.parseGraphql(readQueryFile("get-product-by-id.graphql"), null);

        ObjectNode response = RestAssured.given()

                .baseUri("https://graphql.anilist.co")
                .body(body)
                .contentType(ContentType.JSON)
                .when()
                .post("/")
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .as(ObjectNode.class);
        Products product = graphqlMapper.map(response, "products", Products.class);
        System.out.println(product);
    }
    }

