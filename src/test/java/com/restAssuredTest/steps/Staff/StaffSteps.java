package com.restAssuredTest.steps.Staff;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.restAssuredTest.data.TestData;
import com.restAssuredTest.model.staff.Staff;
import com.restAssuredTest.utils.GraphqlMapper;
import com.vimalselvam.graphql.GraphqlTemplate;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static com.restAssuredTest.utils.FileUtils.readQueryFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class StaffSteps {

    @Autowired
    private TestData testData;

    @Autowired
    private GraphqlMapper graphqlMapper;

    private ObjectMapper objectMapper = new ObjectMapper();


    @When("I get the staff with id {int}")
    public void iGetTheStaffWithId(int staffId) throws IOException {

//                ObjectNode objectNodeAlt = objectMapper.createObjectNode();
//        objectNodeAlt.put("id", staffId);

        Staff createStaffRequest = Staff.builder()
                .id(staffId)
                .build();


        ObjectNode objectNode = graphqlMapper.map(createStaffRequest);

        String body = GraphqlTemplate.parseGraphql(readQueryFile("get-staff-by-id.graphql"), objectNode);

        ObjectNode response = RestAssured.given()

                .baseUri("https://graphql.anilist.co")
                .body(body)
                .contentType(ContentType.JSON)
                .when()
                .post("/")
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .as(ObjectNode.class);

        Staff staff = graphqlMapper.map(response, "Staff", Staff.class);

        System.out.println(staff);
//        assertThat()
    }

    @When("I get the staff by searching the word {int}")
    public void iGetTheStaffBySearchingTheWord(int staffId) throws IOException {

        ObjectNode objectNodeAlt = objectMapper.createObjectNode();
        objectNodeAlt.put("id", staffId);

//        ObjectNode objectNode = graphqlMapper.map(objectNodeAlt);

        String body = GraphqlTemplate.parseGraphql(readQueryFile("get-staff-name-by-id.graphql"), objectNodeAlt);

        ObjectNode response = RestAssured.given()

                .baseUri("https://graphql.anilist.co")
                .body(body)
                .contentType(ContentType.JSON)
                .when()
                .post("/")
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .as(ObjectNode.class);

        Staff staff = graphqlMapper.map(response, "Staff", Staff.class);

        System.out.println(staff);
        assertThat(staff.getId(),is(notNullValue()));
        assertThat(staff.getName().getFirst(),is(notNullValue()));
        assertThat(staff.getName().getLast(),is(notNullValue()));
        assertThat(staff.getName().getFull(),is(notNullValue()));
        assertThat(staff.getLanguage(),is("JAPANESE"));
    }


}
